package com.orudream.servershutup;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ServerShutup extends JavaPlugin {
    @Override
    public void onEnable() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new NoiseListener(), this);
    }

    @Override
    public void onDisable() {
    }

}
