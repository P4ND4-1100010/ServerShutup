package com.orudream.servershutup;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NoiseListener implements Listener {
    @EventHandler
    private void onPlayerJoin(PlayerJoinEvent pje) {
        pje.setJoinMessage(null);
    }

    @EventHandler
    private void onPlayerQuit(PlayerQuitEvent pqe) {
        pqe.setQuitMessage(null);
    }
}
